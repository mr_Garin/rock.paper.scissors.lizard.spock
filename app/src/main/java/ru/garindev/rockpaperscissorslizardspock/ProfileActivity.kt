package ru.garindev.rockpaperscissorslizardspock

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth


class ProfileActivity : AppCompatActivity() {

    val displayName : TextView by lazy { findViewById(R.id.pf_displayed_name) }
    val email : TextView by lazy { findViewById(R.id.pf_email) }
    val logOutButton : Button by lazy { findViewById(R.id.pf_logout) }
    val user = FirebaseAuth.getInstance().currentUser!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        loadProfileData()
    }

    fun loadProfileData(){
        displayName.text = user?.displayName
        email.text = user?.email
    }

    fun logOut(view : View){
        AuthUI.getInstance().signOut(this).addOnCompleteListener {
            finish()
        }
    }
}