package ru.garindev.rockpaperscissorslizardspock.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.garindev.rockpaperscissorslizardspock.R
import ru.garindev.rockpaperscissorslizardspock.data.ScoreItem


class LeaderBoardAdapter(private val scores : List<ScoreItem>) :
    RecyclerView.Adapter<LeaderBoardAdapter.ViewHolder>() {


    class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {

        var name :TextView = item.findViewById(R.id.lb_rv_name)
        var value :TextView = item.findViewById(R.id.lb_rv_value)
        var item :View = item.findViewById(R.id.lb_rv_item)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val item = LayoutInflater.from(parent.context)
            .inflate(R.layout.leader_board_item, parent, false)
        return ViewHolder(item)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = scores[position].userName
        holder.value.text = (scores[position].topCombo).toString()
        if (position %2 == 0){
            holder.item.setBackgroundColor(Color.CYAN)
        }
        else holder.item.setBackgroundColor(Color.MAGENTA)
    }

    override fun getItemCount(): Int {
        return  scores.size
    }
}