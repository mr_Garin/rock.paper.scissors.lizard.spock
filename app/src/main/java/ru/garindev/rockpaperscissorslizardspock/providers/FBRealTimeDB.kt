package ru.garindev.rockpaperscissorslizardspock.providers

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase

object FBRealTimeDB {

    private val database: DatabaseReference by lazy {Firebase.database(fbUrl).reference}
    private const val fbUrl = "https://rockpaperscissorslizards-f3c4b-default-rtdb.europe-west1.firebasedatabase.app/"
    private const val MULTIPLAYER = "multiPlayer"

    fun getObservableChoice(gameSession: String, userSide: String): LiveData<String> {

        val choice : MutableLiveData<String> = MutableLiveData()

        val rivalListener = object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                choice.value =  snapshot.child("choice").getValue<String>()
            }

            override fun onCancelled(error: DatabaseError) {
//              TODO("Not yet implemented")
            }
        }

        database.child(MULTIPLAYER).child(gameSession).child(userSide)
            .addValueEventListener(rivalListener)
        return choice
    }

    fun postChoice(choice: String, gameSession: String, userSide: String){
        database.child(MULTIPLAYER).child(gameSession).child(userSide).setValue(mapOf(
            "choice" to choice))
    }

    fun resetDB(gameSession: String){
        database.child(MULTIPLAYER).child(gameSession).child("host").setValue(mapOf(
            "choice" to ""))
        database.child(MULTIPLAYER).child(gameSession).child("client").setValue(mapOf(
            "choice" to ""))
    }
}