package ru.garindev.rockpaperscissorslizardspock

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.children
import androidx.core.view.get
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import ru.garindev.rockpaperscissorslizardspock.data.ScoreItem
import ru.garindev.rockpaperscissorslizardspock.providers.FBRealTimeDB
import kotlin.random.Random

class MainActivity() : AppCompatActivity() {
    private lateinit var database: DatabaseReference
    lateinit var rockBtn: View
    lateinit var scissorsBtn: View
    lateinit var paperBtn: View
    lateinit var lizardBtn: View
    lateinit var spockBtn: View
    lateinit var repeatBtn: View
    lateinit var icUserChoice: ImageView
    lateinit var icCompChoice: ImageView
    lateinit var textResult: TextView
    lateinit var textTotalWins: TextView
    lateinit var textTotalDraws: TextView
    lateinit var textTotalLose: TextView
    lateinit var textCombo: TextView
    var topCombo = 0
    var totalWins = 0
    var totalDraws = 0
    var totalLose = 0
    var combo = 0

    private lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initializer()
    }

    override fun onPause() {

        pushTopScore()

        super.onPause()
    }

    private fun initializer(){
        rockBtn = findViewById(R.id.rock_Btn)
        scissorsBtn = findViewById(R.id.scissors_btn)
        paperBtn = findViewById(R.id.paper_Btn)
        lizardBtn = findViewById(R.id.lizard_Btn)
        spockBtn = findViewById(R.id.spock_Btn)
        repeatBtn = findViewById(R.id.repeat_Btn)
        icUserChoice = findViewById(R.id.user_Choice)
        icCompChoice = findViewById(R.id.comp_Choice)
        textResult = findViewById(R.id.result)
        textTotalWins = findViewById(R.id.totalWins_tv)
        textTotalDraws = findViewById(R.id.totalDraw_tv)
        textTotalLose = findViewById(R.id.totalLose_tv)
        textCombo = findViewById(R.id.combo_tv)

        setTotals()

        mAuth = FirebaseAuth.getInstance()

        database = FirebaseDatabase.getInstance("https://rockpaperscissorslizards-f3c4b-default-rtdb.europe-west1.firebasedatabase.app/").reference

        getUserTopCombo()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {

        if(FirebaseAuth.getInstance().currentUser != null)
        menu?.get(2)?.setTitle(FirebaseAuth.getInstance().currentUser?.displayName)
        invalidateOptionsMenu()
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId){
            R.id.menu_rules ->{
                val intent = Intent(this, HelpActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.menu_user_profile -> {
                if (mAuth.currentUser == null){
                    startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder().build(), 100)
                } else {
                    val intent = Intent(this, ProfileActivity::class.java)
                    startActivity(intent)
                }
                return true
            }
            R.id.menu_leader_board ->{
                val intent  = Intent(this, LeaderBoard::class.java)
                startActivity(intent)
                return true
            }
            R.id.menu_multiplayer ->{
                val intent = Intent(this, MultiPlayerActivity::class.java)
                startActivity(intent)
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    fun getUserChoice(view: View){

        when(view.id){
            R.id.rock_Btn -> getWinner("Rock", getCompChoice())
            R.id.scissors_btn -> getWinner("Scissors", getCompChoice())
            R.id.paper_Btn -> getWinner("Paper", getCompChoice())
            R.id.lizard_Btn -> getWinner("Lizard", getCompChoice())
            R.id.spock_Btn -> getWinner("Spock", getCompChoice())
        }
    }

    fun getCompChoice(): String{
        val choiceVariants = arrayOf("Rock", "Scissors", "Paper", "Lizard", "Spock")
        val randomNum = Random.nextInt(0, 4)
        return choiceVariants[randomNum]
    }

    fun getWinner(userChoice: String, compChoice: String) {
        blockUserChoice()
        if (userChoice != compChoice) {
            when (userChoice) {
                "Rock" -> {
                    if (compChoice == "Lizard" || compChoice == "Scissors")
                        setResults(userChoice, compChoice, getString(R.string.win))
                    else
                        setResults(userChoice, compChoice, getString(R.string.lose))

                }
                "Scissors" -> {
                    if (compChoice == "Paper" || compChoice == "Lizard")
                        setResults(userChoice, compChoice, getString(R.string.win))
                    else
                        setResults(userChoice, compChoice, getString(R.string.lose))
                }
                "Paper" -> {
                    if (compChoice == "Rock" || compChoice == "Spock")
                        setResults(userChoice, compChoice, getString(R.string.win))
                    else
                        setResults(userChoice, compChoice, getString(R.string.lose))
                }
                "Lizard" -> {
                    if (compChoice == "Spock" || compChoice == "Paper")
                        setResults(userChoice, compChoice, getString(R.string.win))
                    else
                        setResults(userChoice, compChoice, getString(R.string.lose))
                }
                "Spock" -> {
                    if (compChoice == "Scissors" || compChoice == "Rock")
                        setResults(userChoice, compChoice, getString(R.string.win))
                    else
                        setResults(userChoice, compChoice, getString(R.string.lose))
                }
            }
        } else
            setResults(userChoice, compChoice, getString(R.string.draw))
    }

    fun setResults(userChoice: String, compChoice: String, result: String){
        setImages(icUserChoice, userChoice)
        setImages(icCompChoice, compChoice)
        textResult.text = result
        when(result){
            getString(R.string.win) -> {
                totalWins++
                combo++
                showCombo(combo)
                textTotalWins.text = getString(R.string.total_wins, totalWins)
            }
            getString(R.string.lose) -> {
                totalLose++
                combo = 0
                unShowCombo()
                textTotalLose.text = getString(R.string.total_lose, totalLose)
            }
            getString(R.string.draw) -> {
                totalDraws++
                textTotalDraws.text = getString(R.string.total_draws, totalDraws)
            }
        }
    }

    fun setImages(imageView: ImageView, choice: String){
        when(choice){
            "Rock" -> imageView.setImageResource(R.drawable.ic_rock)
            "Scissors" -> imageView.setImageResource(R.drawable.ic_scissors)
            "Paper" -> imageView.setImageResource(R.drawable.ic_paper)
            "Lizard" -> imageView.setImageResource(R.drawable.ic_lizard)
            "Spock" -> imageView.setImageResource(R.drawable.ic_spock)
        }
    }

    fun blockUserChoice(){
        rockBtn.visibility = View.GONE
        scissorsBtn.visibility = View.GONE
        paperBtn.visibility = View.GONE
        lizardBtn.visibility = View.GONE
        spockBtn.visibility = View.GONE
        repeatBtn.visibility = View.VISIBLE
        icUserChoice.visibility = View.VISIBLE
        icCompChoice.visibility = View.VISIBLE
        textResult.visibility = View.VISIBLE
    }

    fun unBlockUserChoice(view: View){
        repeatBtn.visibility = View.GONE
        icUserChoice.visibility = View.GONE
        icCompChoice.visibility = View.GONE
        textResult.visibility = View.GONE
        rockBtn.visibility = View.VISIBLE
        scissorsBtn.visibility = View.VISIBLE
        paperBtn.visibility = View.VISIBLE
        lizardBtn.visibility = View.VISIBLE
        spockBtn.visibility = View.VISIBLE
    }

    fun setTotals(){
        textTotalWins.text = resources.getString(R.string.total_wins, totalWins)
        textTotalDraws.text = resources.getString(R.string.total_draws, totalDraws)
        textTotalLose.text = resources.getString(R.string.total_lose, totalLose)
    }

    fun showCombo(i: Int){
        if (i > 1){
            textCombo.text = getString(R.string.combo, i)
            textCombo.visibility = View.VISIBLE
            if (i > topCombo){
                topCombo = i
            }
        }
    }

    fun unShowCombo(){
        textCombo.visibility = View.GONE
    }

    fun pushTopScore() {

        if(mAuth.currentUser == null && topCombo == 0){
            return
        }

        database.child("leaderBoard").child(mAuth.currentUser!!.uid).get().addOnSuccessListener { it ->
            if (it.value == null){
                database.child("leaderBoard").child(mAuth.currentUser!!.uid).setValue(ScoreItem(mAuth.currentUser?.email!!,
                    mAuth.currentUser?.displayName!!,
                    topCombo).toMap())
            } else{
                if ((it.child("topCombo").value as Long).toInt() <= topCombo) {
                    database.child("leaderBoard").child(mAuth.currentUser!!.uid).setValue(
                        ScoreItem(
                            mAuth.currentUser?.email!!,
                            mAuth.currentUser?.displayName!!,
                            topCombo
                        ).toMap()
                    )
                }
            }
        }.addOnFailureListener {
            Log.d("TAG", "error getting data $it")
        }
    }

    fun getUserTopCombo(){

        if(mAuth.currentUser == null){
            return
        }

        database.child("leaderBoard").child(mAuth.currentUser!!.uid).child("topCombo").get().addOnSuccessListener { it ->
            if(it.value != null) {
                Log.d("TAG", "database mesg: $it")
                topCombo = (it.value as Long).toInt()
                Log.d("TAG", "top combo: $topCombo")
            }
        }.addOnFailureListener {
            Log.d("TAG", "error getting data $it")
        }
    }
}