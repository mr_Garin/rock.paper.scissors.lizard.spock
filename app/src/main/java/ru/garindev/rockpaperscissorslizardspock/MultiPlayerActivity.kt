package ru.garindev.rockpaperscissorslizardspock

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.content.getSystemService
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.google.firebase.auth.FirebaseAuth
import ru.garindev.rockpaperscissorslizardspock.providers.FBRealTimeDB

class MultiPlayerActivity  : AppCompatActivity() {

    val hostUidInput : TextView by lazy { findViewById(R.id.input_host_uid) }
    val connectBtn : ImageButton by lazy { findViewById(R.id.mp_connect_to) }
    val createSessionBTN : Button by lazy { findViewById(R.id.create_new_mp_session) }
    val rockBtn : ImageButton by lazy { findViewById(R.id.mp_rock_Btn) }
    val scissorsBtn : ImageButton by lazy { findViewById(R.id.mp_scissors_btn) }
    val paperBtn : ImageButton by lazy { findViewById(R.id.mp_paper_Btn) }
    val lizardBtn : ImageButton by lazy { findViewById(R.id.mp_lizard_Btn) }
    val spockBtn : ImageButton by lazy { findViewById(R.id.mp_spock_Btn) }
    val mAuth : FirebaseAuth by lazy { FirebaseAuth.getInstance() }
    val icUserChoice: ImageView by lazy { findViewById(R.id.mp_user_choice) }
    val icRivalChoice: ImageView by lazy { findViewById(R.id.mp_rival_choice) }
    val textResult: TextView by lazy { findViewById(R.id.mp_result) }
    val textTotalWins: TextView by lazy { findViewById(R.id.mp_totalWins_tv) }
    val textTotalDraws: TextView by lazy { findViewById(R.id.mp_totalDraw_tv) }
    val textTotalLose: TextView by lazy { findViewById(R.id.mp_totalLose_tv) }
    val textCombo: TextView by lazy { findViewById(R.id.mp_combo_tv) }
    val textSession : TextView by lazy { findViewById(R.id.mp_game_session) }
    lateinit var mpRivalSide : String
    lateinit var mpSide : String
    lateinit var gameSession : String
    var choice : String? = null
    var answer : LiveData<String>? = null
    var totalWins = 0
    var totalDraws = 0
    var totalLose = 0
    var combo = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multi_player)

        init()
    }

    fun init(){
        if (mAuth.currentUser == null){
            createSessionBTN.isEnabled = false
            createSessionBTN.text = getText(R.string.mp_connect_no_auth)
        }
    }

    fun disableConnectionBtns(){
        connectBtn.isEnabled = false
        createSessionBTN.isEnabled = false
    }

    fun goneSessionViews(){
        hostUidInput.visibility = View.GONE
        connectBtn.visibility = View.GONE
        createSessionBTN.visibility = View.GONE
    }

    fun setupGameViews(){
        rockBtn.visibility = View.VISIBLE
        scissorsBtn.visibility = View.VISIBLE
        paperBtn.visibility = View.VISIBLE
        lizardBtn.visibility = View.VISIBLE
        spockBtn.visibility = View.VISIBLE
        icUserChoice.visibility = View.VISIBLE
        icRivalChoice.visibility = View.VISIBLE
        textResult.visibility = View.VISIBLE
        textResult.text = ""
        textTotalWins.visibility = View.VISIBLE
        textTotalDraws.visibility = View.VISIBLE
        textTotalLose.visibility = View.VISIBLE
        textTotalWins.text = getString(R.string.total_wins, totalWins)
        textTotalLose.text = getString(R.string.total_lose, totalLose)
        textTotalDraws.text = getString(R.string.total_draws, totalDraws)
    }

    fun initGame(gameSession: String, rivalSide: String){
        goneSessionViews()
        setupGameViews()
        answer = FBRealTimeDB.getObservableChoice(gameSession, rivalSide)
        answer!!.observe(this, Observer{
            if (it != ""){
                if (mpSide == "host"){
                    if (choice != null) {
                        getWinner(choice!!, it)
                    }
                } else{
                    setImages(icRivalChoice, "None")
                    enableChoice()
                }
            }
        })
    }

    fun connect(v: View?) {
        disableConnectionBtns()
        when(v?.id){
            R.id.mp_connect_to ->{
//              it's client's side of logic
                mpSide = "client"
                mpRivalSide = "host"
                gameSession = hostUidInput.text.toString()
                initGame(gameSession, mpRivalSide)
                disableChoice()
                hideKeyBoard()
            }

            R.id.create_new_mp_session ->{
//                it's host's side of logic
                mpSide = "host"
                mpRivalSide = "client"
                gameSession = mAuth.currentUser!!.uid
//                gameSession = "123"
                textSession.visibility = View.VISIBLE
                textSession.text = gameSession
                initGame(gameSession, mpRivalSide)
            }
        }
    }

    fun hideKeyBoard(){
        getSystemService<InputMethodManager>()!!
            .toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
    }

    fun getChoice(v: View){
        disableChoice()
        when(v.id){
            R.id.mp_rock_Btn -> {
                FBRealTimeDB.postChoice("Rock", gameSession, mpSide)
//                if (mpSide == "host")
                    choice = "Rock"
                if(mpSide == "client")  getWinner("Rock", answer?.value!!)
            }
            R.id.mp_scissors_btn -> {
                FBRealTimeDB.postChoice("Scissors", gameSession, mpSide)
//                if (mpSide == "host")
                    choice = "Scissors"
                if(mpSide == "client") getWinner("Scissors", answer?.value!!)
            }
            R.id.mp_paper_Btn -> {
                FBRealTimeDB.postChoice("Paper", gameSession, mpSide)
//                if (mpSide == "host")
                    choice = "Paper"
                if(mpSide == "client") getWinner("Paper", answer?.value!!)
            }
            R.id.mp_lizard_Btn -> {
                FBRealTimeDB.postChoice("Lizard", gameSession, mpSide)
//                if (mpSide == "host")
                    choice = "Lizard"
                if(mpSide == "client") getWinner("Lizard", answer?.value!!)
            }
            R.id.mp_spock_Btn -> {
                FBRealTimeDB.postChoice("Spock", gameSession, mpSide)
//                if (mpSide == "host")
                    choice = "Spock"
                if(mpSide == "client") getWinner("Spock", answer?.value!!)
            }
        }
        setImages(icUserChoice, choice!!)
        if (mpSide == "host") {
            setImages(icRivalChoice, "None")
        }
    }

    fun getWinner(userChoice: String, rivalChoice : String){
        if (userChoice != rivalChoice) {
            when (userChoice) {
                "Rock" -> {
                    if (rivalChoice == "Lizard" || rivalChoice == "Scissors")
                        setResults(userChoice, rivalChoice, getString(R.string.win))
                    else
                        setResults(userChoice, rivalChoice, getString(R.string.lose))

                }
                "Scissors" -> {
                    if (rivalChoice == "Paper" || rivalChoice == "Lizard")
                        setResults(userChoice, rivalChoice, getString(R.string.win))
                    else
                        setResults(userChoice, rivalChoice, getString(R.string.lose))
                }
                "Paper" -> {
                    if (rivalChoice == "Rock" || rivalChoice == "Spock")
                        setResults(userChoice, rivalChoice, getString(R.string.win))
                    else
                        setResults(userChoice, rivalChoice, getString(R.string.lose))
                }
                "Lizard" -> {
                    if (rivalChoice == "Spock" || rivalChoice == "Paper")
                        setResults(userChoice, rivalChoice, getString(R.string.win))
                    else
                        setResults(userChoice, rivalChoice, getString(R.string.lose))
                }
                "Spock" -> {
                    if (rivalChoice == "Scissors" || rivalChoice == "Rock")
                        setResults(userChoice, rivalChoice, getString(R.string.win))
                    else
                        setResults(userChoice, rivalChoice, getString(R.string.lose))
                }
            }
        } else
            setResults(userChoice, rivalChoice, getString(R.string.draw))
    }

    fun setResults(userChoice: String, rivalChoice: String, result: String){
        setImages(icRivalChoice, rivalChoice)
        textResult.text = result
        when(result){
            getString(R.string.win) -> {
                totalWins++
                combo++
                showCombo(combo)
                textTotalWins.text = getString(R.string.total_wins, totalWins)
            }
            getString(R.string.lose) -> {
                totalLose++
                combo = 0
                unShowCombo()
                textTotalLose.text = getString(R.string.total_lose, totalLose)
            }
            getString(R.string.draw) -> {
                totalDraws++
                textTotalDraws.text = getString(R.string.total_draws, totalDraws)
            }
        }
        if (mpSide == "host") {
            FBRealTimeDB.resetDB(gameSession)
            enableChoice()
        }
    }

    fun setImages(imageView: ImageView, choice: String){
        when(choice){
            "Rock" -> imageView.setImageResource(R.drawable.ic_rock)
            "Scissors" -> imageView.setImageResource(R.drawable.ic_scissors)
            "Paper" -> imageView.setImageResource(R.drawable.ic_paper)
            "Lizard" -> imageView.setImageResource(R.drawable.ic_lizard)
            "Spock" -> imageView.setImageResource(R.drawable.ic_spock)
            "None" -> imageView.setImageResource(R.drawable.round_btns)
        }
    }

    fun showCombo(i: Int){
        if (i > 1){
            textCombo.text = getString(R.string.combo, i)
            textCombo.visibility = View.VISIBLE
        }
    }

    fun unShowCombo(){
        textCombo.visibility = View.GONE
    }

    fun disableChoice(){
        rockBtn.isEnabled = false
        scissorsBtn.isEnabled = false
        paperBtn.isEnabled = false
        lizardBtn.isEnabled = false
        spockBtn.isEnabled = false
    }

    fun enableChoice(){
        rockBtn.isEnabled = true
        scissorsBtn.isEnabled = true
        paperBtn.isEnabled = true
        lizardBtn.isEnabled = true
        spockBtn.isEnabled = true
    }

    fun copyToClipBoard(v: View){
        getSystemService<ClipboardManager>()
            ?.setPrimaryClip(ClipData.newPlainText("", (v as TextView).text))
        Toast.makeText(this, "Copied to ClipBoard", Toast.LENGTH_SHORT).show()
    }
}