package ru.garindev.rockpaperscissorslizardspock

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import ru.garindev.rockpaperscissorslizardspock.adapters.LeaderBoardAdapter
import ru.garindev.rockpaperscissorslizardspock.data.ScoreItem

class LeaderBoard : AppCompatActivity() {

    private lateinit var database: DatabaseReference
    private val list: MutableList<ScoreItem> = mutableListOf()
    private val revView: RecyclerView by lazy {findViewById(R.id.leader_board_rv)}


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leader_board)

        database = Firebase
            .database("https://rockpaperscissorslizards-f3c4b-default-rtdb.europe-west1.firebasedatabase.app/")
            .reference

        database.child("leaderBoard").orderByChild("topCombo").get().addOnSuccessListener {
//            Log.d("MyLogs", it.value.toString())
            for (postSnapshot in it.children ){
//                Log.d("MyLogs", postSnapshot.toString())
                val post = postSnapshot.getValue<ScoreItem>()
                list.add(post!!)
            }
            initRV()
        }.addOnFailureListener {
            Log.d("MyLogs", it.message!!)
        }
    }

    fun initRV(){
        revView.layoutManager = LinearLayoutManager(this)
        list.reverse()
        revView.adapter = LeaderBoardAdapter(list)
    }
}