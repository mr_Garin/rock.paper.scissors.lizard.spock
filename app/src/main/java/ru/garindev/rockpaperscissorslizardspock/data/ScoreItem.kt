package ru.garindev.rockpaperscissorslizardspock.data

import com.google.firebase.database.Exclude

data class ScoreItem(
    val id: String? = null,
    val userName: String? = null,
    val topCombo: Int? = null
){
    @Exclude
    fun toMap(): Map<String, Any>{
        return mapOf(
            "id" to id!!,
            "userName" to userName!!,
            "topCombo" to topCombo!!
        )
    }
}

